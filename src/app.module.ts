import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './common/modules/database/database.module';
import { UsersModule } from './components/users/users.module';
import { AuthModule } from './components/auth/auth.module';
import { EventsModule } from './components/events/events.module';
import { InvitationsModule } from './components/invitations/invitations.module';
import { ConfigModule } from '@nestjs/config';
import { StatsModule } from './components/stats/stats.module';

const env = process.env.NODE_ENV;

@Module({
  imports: [
    DatabaseModule,
    ConfigModule.forRoot({
      envFilePath: (!env || env === 'prod') ? '.env' : `.${env}.env`,
      isGlobal: true
    }),
    UsersModule,
    AuthModule,
    EventsModule,
    InvitationsModule,
    StatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
