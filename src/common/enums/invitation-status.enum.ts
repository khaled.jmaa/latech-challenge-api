export enum InvitationStatus {
    ACCEPTED = 'accepted',
    NOT_ACCEPTED = 'not_accepted'
}