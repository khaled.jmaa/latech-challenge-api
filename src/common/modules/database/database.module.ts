import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            useFactory: () => ({
                type: 'mysql', // TODO process.env.DB_TYPE,
                port: Number(process.env.DB_PORT),
                host: process.env.DB_HOST,
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE,
                entities: [
                    path.join(__dirname, '../../../**/*.entity{.ts,.js}')
                ],
                synchronize: true,
            })
        })
    ],
    exports: [TypeOrmModule]
})
export class DatabaseModule { }
