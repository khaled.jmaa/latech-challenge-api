import { Body, Controller, HttpStatus, Post, Res, UnauthorizedException, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import * as bcrypt from 'bcrypt';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {

    constructor(
        private authService: AuthService,
        private usersService: UsersService
    ) { }

    @Post('login')
    @UsePipes(ValidationPipe)
    async login(@Res() res, @Body() body: LoginDto) {
        const user = await this.usersService.findOneBy({ email: body.email.toLocaleLowerCase() });
        if (!user) { throw new UnauthorizedException('Invalid cedentials') };

        const pwd = await this.usersService.fetchPasswordByEmail(body.email);
        const isPwdMatch = bcrypt.compareSync(body.password, pwd);

        if (!isPwdMatch) { throw new UnauthorizedException('Invalid cedentials') }

        const token = await this.authService.generateToken(user);

        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'access granted',
                data: { token, user }
            });
    }

    @Post('register')
    @UsePipes(ValidationPipe)
    async register(@Res() res, @Body() body: RegisterDto): Promise<any> {
        let user = await this.usersService.findOneBy({ email: body.email });
        if (user) {
            return res.status(HttpStatus.CONFLICT)
                .json({
                    status: HttpStatus.CONFLICT,
                    message: 'email already used',
                    conflictField: 'email'
                });
        }

        user = await this.usersService.findOneBy({ username: body.username });
        if (user) {
            return res.status(HttpStatus.CONFLICT)
                .json({
                    status: HttpStatus.CONFLICT,
                    message: 'username already used',
                    conflictField: 'username'
                });
        }

        user = await this.usersService.create(body);

        const token = this.authService.generateToken(user);

        return res.status(HttpStatus.CREATED)
            .json({
                status: HttpStatus.CREATED,
                message: 'user registred',
                data: { token, user }
            });
    }
}
