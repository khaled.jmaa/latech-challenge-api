import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { AdminStrategy } from './strategies/admin.strategy';
import { AuthController } from './auth.controller';
import { UserStrategy } from './strategies/user.strategy';
import { UsersModule } from '../users/users.module';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({
      useFactory: () => {
        return {
          secret: process.env.JWT_KEY,
          signOptions: {
            expiresIn: process.env.JWT_EXPIRE_TIME
          },
        };
      }
    }),
  ],
  providers: [
    AuthService,
    AdminStrategy,
    UserStrategy
  ],
  controllers: [AuthController]
})
export class AuthModule { }
