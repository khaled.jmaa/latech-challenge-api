import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IJwtPayload } from '../../common/interfaces/jwt-payload.interface';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {

    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) { }

    async validateUser(payload: IJwtPayload) {
        return await this.usersService.findOneBy({ id: payload.id });
    }

    async generateToken(user) {
        const payload: IJwtPayload = {
            id: user.id,
            email: user.email
        };
        const token = this.jwtService.sign(payload);
        return token;
    }
}
