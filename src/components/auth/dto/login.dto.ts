import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty} from 'class-validator';

export class LoginDto {

    @IsNotEmpty()
    @IsEmail()
    // @ValidateIf(o => !o.username)
    @ApiProperty(
        // { description: 'Email can be not required only if username is provided' }
    )
    email?: string;

    // TODO login by username
    // @IsNotEmpty()
    // @ValidateIf(o => !o.email)
    // @ApiProperty({ description: 'Email can required only if email is not provided' })
    // username?: string;

    @IsNotEmpty()
    @ApiProperty()
    password: string;
}