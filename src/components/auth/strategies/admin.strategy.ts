import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRoles } from '../../../common/enums/user-roles.enum';
import { IJwtPayload } from '../../../common/interfaces/jwt-payload.interface';
import { AuthStrategies } from '../../../common/enums/auth-strategies.enum';
import { AuthService } from '../auth.service';

@Injectable()
export class AdminStrategy extends PassportStrategy(Strategy, AuthStrategies.ADMIN) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_KEY,
        });
    }

    async validate(payload: IJwtPayload) {
        const user = await this.authService.validateUser(payload);

        if (!user) {
            throw new UnauthorizedException();
        }
        if (user.role !== UserRoles.ADMIN) {
            throw new ForbiddenException('Permission denied');
        }
        return user;
    }
}
