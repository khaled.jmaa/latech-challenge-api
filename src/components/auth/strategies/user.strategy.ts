import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IJwtPayload } from '../../../common/interfaces/jwt-payload.interface';
import { AuthStrategies } from '../../../common/enums/auth-strategies.enum';
import { AuthService } from '../auth.service';

@Injectable()
export class UserStrategy extends PassportStrategy(Strategy, AuthStrategies.USER) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_KEY,
        });
    }

    async validate(payload: IJwtPayload) {
        const user = await this.authService.validateUser(payload);

        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
