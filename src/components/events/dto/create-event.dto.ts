import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsDateString} from 'class-validator';

export class CreateEventDto {

    @ApiProperty()
    title: string;

    @ApiProperty()
    category: string;

    @IsNotEmpty()
    @IsDateString()
    @ApiProperty()
    date: string;
}