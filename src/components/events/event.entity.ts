import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Invitation } from '../invitations/invitation.entity';
import { User } from '../users/user.entity';

@Entity({ name: 'events' })
export class Event {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true })
    description: string;

    @Column({ nullable: true })
    category: string;

    @Column({ nullable: true })
    location: string;

    @Column({ nullable: true })
    date: Date;

    @Column({ nullable: true })
    duration: number; // In minutes

    @ManyToOne(type => User, user => user.id)
    creator: User;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updateAt: Date;

    @OneToMany(type => Invitation, invitation => invitation.id)
    invitations: Invitation[];

}