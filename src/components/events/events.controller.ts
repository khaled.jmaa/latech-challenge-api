import { Body, Controller, Get, HttpStatus, Param, Post, Res, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { AuthStrategies } from 'src/common/enums/auth-strategies.enum';
import { CreateEventDto } from './dto/create-event.dto';
import { EventsService } from './events.service';

@ApiTags('Events')
@Controller('events')
export class EventsController {

    constructor(
        private eventsService: EventsService
    ) { }

    @Get()
    @UseGuards(AuthGuard(AuthStrategies.ADMIN))
    async findAll(@Res() res) {
        const events = await this.eventsService.find();
        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data fetched',
                data: events
            })
    }

    @Get(':id')
    @UseGuards(AuthGuard(AuthStrategies.ADMIN))
    async findOne(@Res() res, @Param('id') id) {
        const event = await this.eventsService.findOne(id);
        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data fetched',
                data: event
            })
    }

    @Post()
    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard(AuthStrategies.ADMIN))
    async create(@Res() res, @Body() body: CreateEventDto) {
        const event = await this.eventsService.create(body);
        return res.status(HttpStatus.CREATED)
            .json({
                status: HttpStatus.CREATED,
                message: 'event created',
                data: event
            })
    }
}
