import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEventDto } from './dto/create-event.dto';
import { Event } from './event.entity';

@Injectable()
export class EventsService {

    constructor(
        @InjectRepository(Event)
        private eventsRepository: Repository<Event>
    ) { }

    async findOne(id: number) {
        return await this.eventsRepository.findOneBy({ id });
    }

    async findOneBy(criteria: {}) {
        return await this.eventsRepository.findOneBy(criteria);
    }

    async find(criteria = {}) {
        return await this.eventsRepository.find(criteria);
    }

    async create(event: Event | CreateEventDto): Promise<Event> {
        return await this.eventsRepository.save(event);
    }
}
