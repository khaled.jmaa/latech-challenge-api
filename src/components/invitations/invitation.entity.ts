import { InvitationStatus } from '../../common/enums/invitation-status.enum';
import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToOne, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Event } from '../events/event.entity';
import { User } from '../users/user.entity';

@Entity({ name: 'invitations' })
export class Invitation {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    status: InvitationStatus; //string;

    @ManyToOne(type => Event, event => event.id)
    @JoinColumn()
    event: Event;

    @ManyToOne(type => User, user => user.id)
    @JoinColumn()
    invited: User;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updateAt: Date;

}