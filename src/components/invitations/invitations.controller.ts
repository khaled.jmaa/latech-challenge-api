import { Body, Controller, Get, HttpStatus, Param, Post, Res, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateInvitationDto } from './dto/create-invitation.dto';
import { InvitationsService } from './invitations.service';

@ApiTags('Invitations')
@Controller('invitations')
export class InvitationsController {

    constructor(
        private invitationsService: InvitationsService
    ) { }

    @Get()
    async findAll(@Res() res) {
        const events = await this.invitationsService.find();
        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data fetched',
                data: events
            })
    }

    @Get(':id')
    async findOne(@Res() res, @Param('id') id) {
        const event = await this.invitationsService.findOne(id);
        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data fetched',
                data: event
            })
    }

    @Post()
    @UsePipes(ValidationPipe)
    async create(@Res() res, @Body() body: CreateInvitationDto) {

        const invitation = await this.invitationsService.create(body);

        return res.status(HttpStatus.CREATED)
            .json({
                status: HttpStatus.CREATED,
                message: 'invitation created',
                data: invitation
            })
    }
}
