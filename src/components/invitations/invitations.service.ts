import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventsService } from '../events/events.service';
import { UsersService } from '../users/users.service';
import { CreateInvitationDto } from './dto/create-invitation.dto';
import { Invitation } from './invitation.entity';

@Injectable()
export class InvitationsService {

    constructor(
        @InjectRepository(Invitation)
        private invitationRepository: Repository<Invitation>,
        private usersService: UsersService,
        private eventsService: EventsService
    ) { }

    async findOne(id: number) {
        return await this.invitationRepository.findOneBy({ id });
    }

    async findOneBy(criteria: {}) {
        return await this.invitationRepository.findOneBy(criteria);
    }

    async find(criteria = {}) {
        const events = await this.invitationRepository.find({
            where: criteria,
            relations: ['event', 'invited']
        });
        return events;
    }

    async create(invitationDto: Invitation | CreateInvitationDto): Promise<Invitation> {

        const invited = await this.usersService.findOne(invitationDto.invited);
        const event = await this.eventsService.findOne(invitationDto.event);

        invitationDto.event = event;
        invitationDto.invited = invited;

        const invitation = await this.invitationRepository.save(invitationDto);
        return invitation;
    }
}
