import { Controller, Get, HttpStatus, Query, Res } from '@nestjs/common';
import { InvitationStatus } from '../../common/enums/invitation-status.enum';
import { Between, IsNull, Not } from 'typeorm';
import { EventsService } from '../events/events.service';
import { InvitationsService } from '../invitations/invitations.service';
import { UsersService } from '../users/users.service';

@Controller('stats')
export class StatsController {

    constructor(
        private usersService: UsersService,
        private eventsService: EventsService,
        private invitationsSercice: InvitationsService
    ) { }

    @Get('summary/admin')
    async getAdminSummary(@Res() res, @Query() query) {

        const dateFrom = query.dateFrom;
        const dateTo = query.dateTo;

        console.log('dateFrom = ', dateFrom);
        console.log('dateTo = ', dateTo);



        // Fetch events
        const events = await this.eventsService.find({
            where: {
                updateAt: Between(dateFrom, dateTo),
                // TODO filter by date field date: Between(dateFrom, dateTo),
            }
        });

        // Fetch answers
        const answers = await this.invitationsSercice.find({
            updateAt: Between(dateFrom, dateTo),
            status: Not(IsNull())
        });
        const noAnswers = await this.invitationsSercice.find({
            updateAt: Between(dateFrom, dateTo),
            status: IsNull()
        });

        // Fetch acceptances / unacceptances
        const acceptanceAnswers = answers.filter(answer => answer.status === InvitationStatus.ACCEPTED);
        const unacceptanceAnswers = answers.filter(answer => answer.status === InvitationStatus.NOT_ACCEPTED);
        
        // Fetch acceptors / unacceptors
        let acceptorsUsers = acceptanceAnswers.map(answer => answer.invited);
        let unacceptorsUsers = unacceptanceAnswers.map(answer => answer.invited);

        
        acceptorsUsers = acceptorsUsers.filter((value, index, self) =>
            index === self.findIndex((t) => (
                t.id === value.id
            ))
        );

        unacceptorsUsers = unacceptorsUsers.filter((value, index, self) =>
            index === self.findIndex((t) => (
                t.id === value.id
            ))
        );

        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data found',
                data: {
                    eventsCount: events.length,
                    answersCount: answers.length,
                    noAnswersCount: noAnswers.length,
                    acceptorsUsers,
                    unacceptorsUsers,
                    acceptanceAnswers,
                    unacceptanceAnswers
                }
            })
    }
}
