import { Module } from '@nestjs/common';
import { EventsModule } from '../events/events.module';
import { InvitationsModule } from '../invitations/invitations.module';
import { UsersModule } from '../users/users.module';
import { StatsController } from './stats.controller';
import { StatsService } from './stats.service';

@Module({
  imports: [
    UsersModule,
    EventsModule,
    InvitationsModule
  ],
  controllers: [StatsController],
  providers: [StatsService]
})
export class StatsModule {}
