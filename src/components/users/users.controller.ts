import { Controller, Get, HttpStatus, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { AuthStrategies } from '../../common/enums/auth-strategies.enum';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) { }

    @Get()
    @UseGuards(AuthGuard(AuthStrategies.ADMIN))
    async getAll(@Res() res) {
        const users = await this.usersService.find();
        return res.status(HttpStatus.OK)
            .json({
                status: HttpStatus.OK,
                message: 'data fatched',
                data: users
            })
    }
}
