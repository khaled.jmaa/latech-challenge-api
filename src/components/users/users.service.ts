import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import { RegisterDto } from '../auth/dto/register.dto';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>
    ) { }

    async findOne(id: number) {
        return await this.usersRepository.findOneBy({ id });
    }

    async findOneBy(criteria: {}) {
        return await this.usersRepository.findOneBy(criteria);
    }

    async find(criteria: {} = {}) {
        return await this.usersRepository.find(criteria);
    }

    async create(user: User | RegisterDto): Promise<User> {
        const salt = 10; // TODO process.env.BCRYPT_SALT
        user.password = bcrypt.hashSync(user.password, salt);
        user.email = user.email.toLowerCase();
        user.username = user.username.toLowerCase();
        return await this.usersRepository.save(user);
    }

    async fetchPasswordByEmail(email: string) {
        const queryRes = await this.usersRepository
            .createQueryBuilder()
            .select('password')
            .where({ email })
            .getRawOne();
        return queryRes.password;
    }
}
